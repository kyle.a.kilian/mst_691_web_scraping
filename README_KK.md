## (Notional) Title: 

**Measuring Chinese Polarization Toward US Diplomatic Actions - Sentiment Analysis

## Description

This project will analyze the variations in China's public opinion and policy sentiment surrounding US pronouncmments on Taiwan. The tool will scrape textual data from leading Chinese Communist Party publications and analyze the polarity of speech patterns surrounding US diplomatic moves toward Taiwan. 

## Visuals
None at this time

## Installation

For this project we'll need access to Github depo, have git installed on the system, and Jupyterhup. Possible librairies we're considering for instalation with this tool are **Beautiful Soup, Selennium, or Tweepy. For the natural language processing, the potential librairies include: **NLTK, TextBlob, PolyGlot, CoreNLP, and Scikit-learn

## Usage

For this project, the tool will measure differences in polarization among Chinese public opinion and CCP official announcements surrounding US diplomatic communication about Taiwan. For example, looking at public statements by US Presidents where the official recognizes Taiwan as an independent entity, or at least tangentially, the tool will analyze Chinese CCP communiations to look for changes in tone, polarity, or agression. We will analyze communications events spanning two adminstrations.  

## Support

For help with this tool, please reach out to the authors: "Ettinger, Matthew J CTR DIA (US)" <Matthew.Ettinger@dodiis.mil>; "Russell, Eric S CIV US)" <Eric.Russell@dodiis.mil>; "Smith, Jered M MAJ USAF DIA (US)" <Jered.Smith@dodiis.mil>; or "Bendt, Robert B MIL DIA (US)" <Robert.Bendt@dodiis.mil>; or "Kilian, Kyle A CIV DIA (US)" <Kyle.Kilian@dodiis.mil>

## Roadmap

The initial roadmap for this project is as follows:
1) Research the issue, to understand common reaction from China to US diplomatic communications
2) Identify the most prominent public, english language, reporting where public officials releast statements
3) Use one of the webscraping tools to mine a representative sampling of statements that span two presidencies, during two events where the US at least partially recongized Taiwan (accidentally, or purposefully) 
4) Organize the scraped files and provide structure and cleaning if needed prior to sentiment analsysis. 
5) From the collected files, run the sentiment analysis on each collection around each event to measure speech polarization, before and after the US action.
4) Organize, Clean and structure the data for analysis
6) Analyze the data, prepare visualization, and present the product. 

## Contributing

We are open for contribution and recommendations on this analysis to better refine the methodology. 

## Authors and acknowledgment

This project was a coordinated effort between Matt Ettinger, Eric Russell, Jared Smith, Robert Bendt, and Kyle Kilian. Emails of the authors below:

<Matthew.Ettinger@dodiis.mil>; <Eric.Russell@dodiis.mil>; <Jered.Smith@dodiis.mil>;  <Robert.Bendt@dodiis.mil>; <Kyle.Kilian@dodiis.mil>

## License
NA

## Project status

This project is just in the early stages and we are still organizing our methodological process, tools, and data sources. 

